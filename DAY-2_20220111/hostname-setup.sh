#!/bin/bash

# Setup Hostname 
sudo hostnamectl set-hostname "ansible.softobiz.com"

# Update the hostname part of Host File
echo "`hostname -I | awk '{ print $1 }'` `hostname`" >> /etc/hosts 
