#!/bin/bash 

WebServer Of Linux - Ubuntu 20.04

aws ec2 run-instances \
--image-id "ami-0851b76e8b1bce90b" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-c41ff7af" \
--security-group-ids "sg-0ab128f87abe85379" \
--tag-specifications --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=TomcatNode2},{Key=Environment,Value=Development},{Key=ProjectName,Value=SoftoBizDevOps},{Key=ProjectID,Value=20220110},{Key=EmailID,Value=devops@softobiz.com},{Key=MobileNo,Value=+919140774908}]' \
--key-name "softodevopss" \
--user-data file://tomcat-hostname.txt --profile sugandhdevops 
