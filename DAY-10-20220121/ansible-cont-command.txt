ubuntu@ansible-controller:~$ cat /etc/hosts
127.0.0.1 localhost

# The following lines are desirable for IPv6 capable hosts
::1 ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
172.31.35.62 ansible-controller.softobiz.com
ubuntu@ansible-controller:~$ ansible --version
ansible [core 2.12.1]
  config file = /etc/ansible/ansible.cfg
  configured module search path = ['/home/ubuntu/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3/dist-packages/ansible
  ansible collection location = /home/ubuntu/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/bin/ansible
  python version = 3.8.10 (default, Sep 28 2021, 16:10:42) [GCC 9.3.0]
  jinja version = 2.10.1
  libyaml = True
ubuntu@ansible-controller:~$ ls -lrt /etc/ansible/hosts
-rw-r--r-- 1 root root 1018 Dec  6 19:26 /etc/ansible/hosts
ubuntu@ansible-controller:~$ cp -pv /etc/ansible/hosts /etc/ansible/hosts_20220121
'/etc/ansible/hosts' -> '/etc/ansible/hosts_20220121'
cp: cannot create regular file '/etc/ansible/hosts_20220121': Permission denied
ubuntu@ansible-controller:~$ sudo cp -pv /etc/ansible/hosts /etc/ansible/hosts_20220121
'/etc/ansible/hosts' -> '/etc/ansible/hosts_20220121'
ubuntu@ansible-controller:~$ sudo ls -lrt /etc/ansible/hosts
-rw-r--r-- 1 root root 1018 Dec  6 19:26 /etc/ansible/hosts
ubuntu@ansible-controller:~$ sudo ls -lrt /etc/ansible/hosts*
-rw-r--r-- 1 root root 1018 Dec  6 19:26 /etc/ansible/hosts_20220121
-rw-r--r-- 1 root root 1018 Dec  6 19:26 /etc/ansible/hosts
ubuntu@ansible-controller:~$ 
ubuntu@ansible-controller:~$ 
ubuntu@ansible-controller:~$ ls -lrta
total 32
-rw-r--r-- 1 ubuntu ubuntu  807 Feb 25  2020 .profile
-rw-r--r-- 1 ubuntu ubuntu 3771 Feb 25  2020 .bashrc
-rw-r--r-- 1 ubuntu ubuntu  220 Feb 25  2020 .bash_logout
drwxr-xr-x 3 root   root   4096 Jan 22 18:35 ..
drwx------ 2 ubuntu ubuntu 4096 Jan 22 18:35 .ssh
drwx------ 2 ubuntu ubuntu 4096 Jan 22 18:37 .cache
drwxrwxr-x 3 ubuntu ubuntu 4096 Jan 22 18:38 .ansible
-rw-r--r-- 1 ubuntu ubuntu    0 Jan 22 18:40 .sudo_as_admin_successful
drwxr-xr-x 5 ubuntu ubuntu 4096 Jan 22 18:40 .
ubuntu@ansible-controller:~$ 
ubuntu@ansible-controller:~$ 
ubuntu@ansible-controller:~$ cd .ssh
ubuntu@ansible-controller:~/.ssh$ ls -lrta
total 12
-rw------- 1 ubuntu ubuntu  394 Jan 22 18:35 authorized_keys
drwx------ 2 ubuntu ubuntu 4096 Jan 22 18:35 .
drwxr-xr-x 5 ubuntu ubuntu 4096 Jan 22 18:40 ..
ubuntu@ansible-controller:~/.ssh$ 
ubuntu@ansible-controller:~/.ssh$ pwd
/home/ubuntu/.ssh
ubuntu@ansible-controller:~/.ssh$ 
ubuntu@ansible-controller:~/.ssh$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/ubuntu/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/ubuntu/.ssh/id_rsa
Your public key has been saved in /home/ubuntu/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:iub/OUWfwgxLH5WU89BUYFTKLjL/34/OWW6n0JpZZqw ubuntu@ansible-controller.softobiz.com
The key's randomart image is:
+---[RSA 3072]----+
|           ..*=+o|
|            *o.. |
|           . +o  |
|        o o  ..  |
|       .SBoo...  |
|     . .. *+o+   |
|    o .  . .o * .|
|   o    ..   % =+|
|    ....o.  E.B+B|
+----[SHA256]-----+
ubuntu@ansible-controller:~/.ssh$ 
ubuntu@ansible-controller:~/.ssh$ pwd
/home/ubuntu/.ssh
ubuntu@ansible-controller:~/.ssh$ 
ubuntu@ansible-controller:~/.ssh$ ls -lrta
total 20
-rw------- 1 ubuntu ubuntu  394 Jan 22 18:35 authorized_keys
drwxr-xr-x 5 ubuntu ubuntu 4096 Jan 22 18:40 ..
-rw-r--r-- 1 ubuntu ubuntu  592 Jan 22 18:43 id_rsa.pub
-rw------- 1 ubuntu ubuntu 2635 Jan 22 18:43 id_rsa
drwx------ 2 ubuntu ubuntu 4096 Jan 22 18:43 .
ubuntu@ansible-controller:~/.ssh$ 
ubuntu@ansible-controller:~/.ssh$ 


ssh 3.6.86.27
The authenticity of host '3.6.86.27 (3.6.86.27)' can't be established.
ECDSA key fingerprint is SHA256:CRS8R5P0H0fJLVp/GJftbyfVW/1b/SgQ6yUP8syYmPQ.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '3.6.86.27' (ECDSA) to the list of known hosts.
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-1022-aws x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sun Jan 23 10:22:26 UTC 2022

  System load:  0.0               Processes:             106
  Usage of /:   20.5% of 7.69GB   Users logged in:       1
  Memory usage: 22%               IPv4 address for eth0: 172.31.37.103
  Swap usage:   0%


1 update can be applied immediately.
To see these additional updates run: apt list --upgradable


The list of available updates is more than a week old.
To check for new updates run: sudo apt update

Last login: Sun Jan 23 10:15:31 2022 from 47.9.125.191
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

ubuntu@web:~$ hostname
web.softobiz.com
ubuntu@web:~$ 

vi timezone-setup.yml 
ubuntu@ansible-controller:~$ cat timezone-setup.yml 
---
- name: Play-1 Setting Hostname 
  hosts: web  # ---> /etc/ansible/hosts [web] publicip or hostname of the webserver
  become: yes
  tasks:
    - name: Update the Ubuntu Repository
      apt: update_cache=yes
      ignore_errors: yes

    - name: Set Timezone 
      command: timedatectl set-timezone Asia/Kolkata
...

ubuntu@ansible-controller:~$ 
ubuntu@ansible-controller:~$ ansible-playbook --syntax-check timezone-setup.yml

playbook: timezone-setup.yml
ubuntu@ansible-controller:~$ 
ubuntu@ansible-controller:~$ ansible-playbook --check timezone-setup.yml

PLAY [Play-1 Setting Hostname] *******************************************************************************************************************************

TASK [Gathering Facts] ***************************************************************************************************************************************
ok: [3.6.86.27]

TASK [Update the Ubuntu Repository] **************************************************************************************************************************

changed: [3.6.86.27]

TASK [Set Timezone] ******************************************************************************************************************************************
skipping: [3.6.86.27]

PLAY RECAP ***************************************************************************************************************************************************
3.6.86.27                  : ok=2    changed=1    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0   

ubuntu@ansible-controller:~$ 
ubuntu@ansible-controller:~$ ansible-playbook timezone-setup.yml

PLAY [Play-1 Setting Hostname] *******************************************************************************************************************************

TASK [Gathering Facts] ***************************************************************************************************************************************

ok: [3.6.86.27]

TASK [Update the Ubuntu Repository] **************************************************************************************************************************
changed: [3.6.86.27]

TASK [Set Timezone] ******************************************************************************************************************************************
changed: [3.6.86.27]

PLAY RECAP ***************************************************************************************************************************************************
3.6.86.27                  : ok=3    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

--------------------> Web Node 1<--------------------------

timedatectl
               Local time: Sun 2022-01-23 16:09:04 IST
           Universal time: Sun 2022-01-23 10:39:04 UTC
                 RTC time: Sun 2022-01-23 10:39:04    
                Time zone: Asia/Kolkata (IST, +0530)  
System clock synchronized: yes                        
              NTP service: active                     
          RTC in local TZ: no                         
ubuntu@web:~$ 
ubuntu@web:~$ timedatectl
               Local time: Sun 2022-01-23 16:09:52 IST
           Universal time: Sun 2022-01-23 10:39:52 UTC
                 RTC time: Sun 2022-01-23 10:39:52    
                Time zone: Asia/Kolkata (IST, +0530)  
System clock synchronized: yes                        
              NTP service: active                     
          RTC in local TZ: no 