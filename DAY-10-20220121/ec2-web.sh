#!/bin/bash 

Ansible Node-1 web - Ubuntu 20.04

aws ec2 run-instances \
--image-id "ami-0851b76e8b1bce90b" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-c41ff7af" \
--security-group-ids "sg-0f7264d9b319d87f0" \
--tag-specifications --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=WebNode},{Key=Environment,Value=Development},{Key=ProjectName,Value=SoftoBizDevOps},{Key=ProjectID,Value=20220114},{Key=EmailID,Value=info@softobiz.com},{Key=MobileNo,Value=+91}]' \
--key-name "softodevopss" \
--user-data file://web-hostname.txt --profile sugandhdevops 