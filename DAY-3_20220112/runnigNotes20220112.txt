Basic & Advanced Commands
2	Scripting - Powershell & Shell
3	File & Directory Mgmt
4	Permissions
5	User & Group Managment
6	File System
7	Package Management
8	Controlling Services & Daemons
9	WebServer - Windows IIS
10	Application Server - Apache Tomcat
11	Database - MSSQL


AWS :

Global Infrastructure : 

1. Regions : 26 
    1.1 84 Availability Zones 

2. 8 Announced Regions
    2.1 >= 16 Availability Zones

3. 17 Local Zones & 20 Wavelength Zones 
    - Announced 30 Local Zones

4. 108 Direct Connect Locations 

5. 310 CDN's PoP's Point of Presence [ 300 Edge Locations, and 13 Reginal Edge Caches ]

200 Fully Featured Services
    - 1400+ sub services 
    - Global & Region based services 
    - Global : IAM, Route53
    - Region : EC2, VPC, 

Two Types of Users :
    1. Root/Super User ---> Who created AWS Account [Do not use Programmatic access]
    2. IAM Users :  AWS Management Console access &  Programmatic access
        - ReadOnly
        - Developers 
        - Admins 

Select AWS credential type

Access key - Programmatic access
Enables an access key ID and secret access key for the AWS API, CLI, SDK, and other development tools.

Password - AWS Management Console access
Enables a password that allows users to sign-in to the AWS Management Console.

Synopsis :
aws [options] <command> <subcommand> [parameters]

Creating a IAM user in AWS :

STEP-1 : Login as SuperUser 

STEP-2 : Go to Services ---> Security, Identity, & Compliance ---> Identity and Access Management (IAM) :

Access management :

User groups : Developers, Opsteam, Operations, engineering, testing, business etc... 
    - Policies : Permissions To Access AWS Services 
    - Add users to Groups 

Users : Create Users and add users to group if group exists or else add Policies directly to users 
    
IAM User : sugandhkumar

Type Of Access :
    1. AWS Management Console access
    2. Programmatic access

Attach Policies To User or Add User to existing Group : 

Roles :

Policies :

Identity providers

Account settings
