#!/bin/bash

# Setup New Hostname
hostnamectl set-hostname "sonarQube.softobiz.com"

# Configure New Hostname as part of /etc/hosts file 
echo "`hostname -I | awk '{ print $1}'` `hostname`" >> /etc/hosts

# Refresh the Terminal
/bin/bash 
