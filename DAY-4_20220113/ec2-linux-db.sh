#!/bin/bash 

WebServer Of Linux - Ubuntu 20.04

aws ec2 run-instances \
--image-id "ami-0851b76e8b1bce90b" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-c41ff7af" \
--security-group-ids "sg-0ab128f87abe85379" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=MySQLDB}]' \
--key-name "softodevopss" \
--user-data file://install-db-mysql.txt --profile sugandhdevops 
