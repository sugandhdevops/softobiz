#!/bin/bash 

WebServer Of Linux - Ubuntu 20.04

aws ec2 run-instances \
--image-id "ami-0851b76e8b1bce90b" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-c41ff7af" \
--security-group-ids "sg-0f7264d9b319d87f0" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=LinuxWebServer}]' \
--key-name "softokey" \
--user-data file://install-web-apache2.txt --profile sugandhdevops 
