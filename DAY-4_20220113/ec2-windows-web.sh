#!/bin/bash 

WebServer Of Windows -2022

aws ec2 run-instances \
--image-id "ami-0dde4a068e559b85f" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-c41ff7af" \
--security-group-ids "sg-0f7264d9b319d87f0" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=WindowsIIS}]' \
--key-name "softodevopss" \
--user-data file://install-web-iis.txt --profile sugandhdevops 
