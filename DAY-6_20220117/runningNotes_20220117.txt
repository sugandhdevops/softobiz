Agenda :

Continuous Integration :
    - Jenkins
    - AWS Cloud :
        - AWS CodeBuild and AWS CodePipeline 
        - AZURE Pipelines 

CI : Is the practice of automating the Integration of code changes from mutliple contiributors into a single software project.

It's a primary DevOps best practice, allowing developers to frequently merge code changes into a
central repository where builds and tests then run.

CI Vendors :

- Jenkins  : https://www.jenkins.io/
- TeamCity 
- Bamboo
- GitLab  
- Travis CI 
- Codeship 
- AWS CodeBuild and AWS CodePipeline 
- AZURE Pipelines 

Software, Software as service & Cloud 
